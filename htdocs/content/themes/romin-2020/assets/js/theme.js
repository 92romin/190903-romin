import $ from 'jquery';

import WOW from 'wow.js';
import _ from 'lodash';
import Rellax from 'rellax';
import { hero } from './components/hero';
import { hamburger } from './components/hamburger';
import { projects } from './components/projects';
import * as functions from './functions.js'

import 'jquery.cookie'
import 'slick-carousel';

$(() => {

  
  /**
   * Testimonials slider.
   */
  $('.section-testimonials__slides').slick({
    infinite: true,
    autoplay: false,
    autoplaySpeed: 4500,
    dots: false,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    rows: 0,
    prevArrow: $('.carousel__prev-testimonials'),
    nextArrow: $('.carousel__next-testimonials'),
  });

  /**
   * Parallax.
   * 
   */
  Rellax('.rellax', {
    center: true,
    horizontal: true
  });

  /**
   * Wow.js (Animations)
   * 
   */
  const wow = new WOW(
    {
      boxClass: 'animate',
      animateClass: 'animated',
      offset: 100,
      mobile: true,
      live: true,
      resetAnimation: true,
    },
  );

  /**
   * Split slides.
   * 
   */
  projects.init({
    slide: 'slide',
    next: '.buttons__next',
    prev: '.buttons__prev',
    parent: '.section-projects',
    container: 'body',
  });

  /**
   * Hero Animations.
   * 
   */
  hero.init({
    heading: 'section-hero__heading',
    overlay: 'section-hero__overlay',
    repeat: true,
    loopWait: 5000,
    headingWait: 2000,
    delayStart: 4000,
    headings: [
      'Websites.',
      'Wordpress.',
      'Web Design.',
      'Dev Ops.',
      'Let\'s Begin!',
    ],
    overlays: [
      'Websites.',
      'Wordpress.',
      'Design',
      'Dev Ops.',
      'Start Now!',
    ],
  });

  $('.hamburger__menu').toggleNavigation()
  $('.menu__item').scrollToSection();

  wow.init();
  functions.setCursor();
  functions.setCookies();
  functions.loadSplashPage();
  functions.pwaIOSDownload();
});

/**
 * On window load & resize.
 * 
 */
$(window).on('load resize', () => {

  hamburger.init({
    xOffset: 0,
    menu: $('.hamburger__menu'),
    menuWrapper: $('.hamburger__wrapper'),
    yPosition: ($('.header__logo').offset().top),
    xPosition: 0,
    responsive: 1024
  });
});

/**
 * On scroll of wrapper.
 * 
 */
$(window).on('scroll DOMMouseScroll mousewheel', () => {

  $('.header__logo').fadeOutOffset(150);
  $('.black-section').changeHeaderColour();
});