<section class="section-sticky-columns @if(get_sub_field('sticky_reverse_columns')) section-sticky-columns--inverse @endif">
    <div class="section-sticky-columns__description">
        {!! get_sub_field('sticky_description') !!}
    </div>

    <div class="section-sticky-columns__images">
        <img class="section-sticky-columns__preview" src="{{ get_sub_field('sticky_image') }}" alt="Preview">
    </div>
</section>
