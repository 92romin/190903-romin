 <section class="[ section-testimonials ] [ black-section ]">
     <div class="section-testimonials__wrapper">
         <div class="section-testimonials__slides [ animate fadeInDown ]">
            @while ( have_rows('testimonial') ) @php(the_row()) 
                <div>
                    <div class="container container__large">
                        <div class="section-testimonials__heading [ animate fadeInDown ]" data-wow-delay="1.5s">What our clients say</div>
                        <div class="testimonial">
                            <div class="testimonial__feedback">
                                {!! get_sub_field('testimonial_feedback') !!}
                            </div>
                            @if(get_sub_field('testimonial_show_client'))
                                <div class="testimonial__client [ animate fadeIn ]" data-wow-delay="0.5s">{{ get_sub_field('testimonial_client') }}</div>
                            @endif
                            @if(get_sub_field('testimonial_show_link'))
                                <a href="{{ get_sub_field('testimonial_link')['url'] }}" class="button button--white-bordered testimonial__cta" @if(get_sub_field('testimonial_link')['target']) target="_blank" @endif>
                                    {{ get_sub_field('testimonial_link')['title'] }} <span class="arrow"></span>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
             @endwhile
         </div>

        @if( count(get_sub_field('testimonial')) > 1 )
         <div class="carousel__controls">
             <div class="carousel__col-2 carousel__col-2__left">
                 <div class="carousel__prev carousel__prev carousel__prev-testimonials">
                     <i class="fas fa-chevron-left"></i>
                 </div>
             </div>
             <div class="carousel__col-2 carousel__col-2--right">
                 <div class="carousel__next carousel__next carousel__next-testimonials">
                     <i class="fas fa-chevron-right"></i>
                 </div>
             </div>
         </div>
        @endif
     </div>
 </section>
