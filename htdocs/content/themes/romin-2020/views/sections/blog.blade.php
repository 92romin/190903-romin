<section class="section-blog">
    <div class="[ overlay ] [ rellax ]">
        <div class="[ overlay__text ] [ animate ]">
            {{ get_sub_field('blog_section_overlay') }}
        </div>
    </div>
    <div class="container__max">
        <div class="column">
            <div class="column column__left">
                <div class="[ information ] [ animate ]">
                    @if(get_sub_field('blog_show_section_sub_heading'))
                        <span class="lead">
                            {{ get_sub_field('blog_section_sub_heading') }}
                        </span>
                    @endif
                    <h1 class="information__heading">
                        {{ get_sub_field('blog_section_heading') }}
                    </h1>
                    <p class="lead information__description">
                        {!! get_sub_field('blog_section_description') !!}
                    </p>
                    @if(get_sub_field('blog_section_link'))
                        <a href="{{ get_sub_field('blog_section_link')['url'] }}" class="button button--white" @if(get_sub_field('blog_section_link')['target']) target="_blank" @endif>
                            {{ get_sub_field('blog_section_link')['title'] }} <span class="arrow"></span>
                        </a>
                    @endif
                </div>
            </div>
            <div class="column column__right">
                <div class="cards">
                    @if(have_rows('blog_posts'))
                        @php($count = 1)
                        @while ( have_rows('blog_posts') ) @php(the_row())
                            @php($post = get_sub_field('post'))
                            <div class="[ card ] [ animate slideInUp ]" @if($count % 2) data-wow-delay="0.25s" @endif>
                                <div class="[ card__single ] @if($count % 2) [ rellax ] @endif"
                                    @if(get_the_post_thumbnail_url($post->ID)) style="background-image: url({{ get_the_post_thumbnail_url($post->ID) }})" @endif 
                                    data-rellax-xs-speed="0"
                                    data-rellax-mobile-speed="0" 
                                    data-rellax-tablet-speed="-0.5"
                                    data-rellax-desktop-speed="-2">
                                    <div class="card__overlay"></div>
                                    <h2 class="card__heading"> {{ $post->post_title }}</h2>
                                    <div class="card__description">
                                        {!! wpautop($post->post_excerpt) !!}
                                    </div>
                                    <a href="{{ $post->guid }}" class="button button--arrow">
                                        View Post <span class="arrow"></span>
                                    </a>
                                </div>
                            </div>
                            @php($count++)
                        @endwhile
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>