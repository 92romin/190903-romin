<section class="section-projects">
        @if( count(get_sub_field('project')) > 1 )
            <div class="navigation">
                <div class="[ progress ] [ animate fadeInUp ]">
                    <div class="progress__current">01</div>
                    <div class="progress__bar-outer">
                        <div class="progress__bar-inner"></div>
                    </div>
                    <div class="total-slides">
                    </div>
                </div>

                <div class="[ buttons ] [ animate fadeInUp ]" data-wow-delay="0.5s">
                    <div class="buttons__prev">
                        <i class="fas fa-chevron-left"></i>
                    </div>
                    <div class="buttons__next">
                        <i class="fas fa-chevron-right"></i>
                    </div>
                </div>
            </div>
        @endif

        @if( have_rows('project') )
        @php($count = 1)
        @php($projectNumber = '')

        <div class="slides">
        @while ( have_rows('project') ) @php(the_row()) 
            @if($count < 10) 
                @php($projectNumber='0' . $count ?? $count) 
            @endif 
            <div class="slide">
                <div class="column column__left">
                    <div class="project">
                        @if(get_sub_field('project_show_sub_heading'))
                        <div class="subhead">
                            <div class="[ subhead__number ] [ animate fadeInUp ]">{{ $projectNumber }}. </div>
                            <div class="[ subhead__separator ] [ animate fadeIn ]" data-wow-delay="0.5s"></div>
                            <span class="[ animate fadeInUp ]" data-wow-delay="1s">{{ get_sub_field('project_sub_heading') }}</span>
                        </div>
                        @endif
                        <h1 class="[ project__heading ] [ animate fadeInUp ]">{{ get_sub_field('project_title') }}</h1>
                        <div class="[ project__description ] [ animate fadeInUp ]" data-wow-delay="0.5s">
                            <div class="lead">
                                {!! get_sub_field('project_description') !!}
                            </div>
                        </div>
                        @if(get_sub_field('project_link'))
                            <div class="project__button">
                                <a href="{{ get_sub_field('project_link')['url'] }}" @if(get_sub_field('project_link')['target']) target="_blank" @endif class="button button--white">
                                    {{ get_sub_field('project_link')['title'] }} <span class="arrow"></span>
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="column column__right">
                   
                    @if(get_sub_field('project_logo'))
                        <img class="project__logo" src="{{ get_sub_field('project_logo') }}" alt="{{ get_sub_field('project_title') }}">
                    @endif

                    <div class="project__preview" style="background-image: url({{ get_sub_field('project_background_image') }})"></div>
                </div>
            </div>
            @php($count++)
        @endwhile

        </div>
    @endif
</section>
