<div class="ios-pwa">
    <div class="ios-pwa__close">
        <i class="fas fa-times"></i>
    </div>
    <div class="ios-pwa__description">
        <span>To install this application on your home screen.</span> 
        <span>Just tap <img class="ios-pwa__share-icon" src="{{ app('wp.theme')->getUrl('assets/images/share.svg') }}"/> then '<strong>Add to Home Screen</strong>'</span>
    </div>
</div>

<div class="ios-pwa__spacer"></div>
