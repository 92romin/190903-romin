@extends('layouts.main')

@section('content')
    @loop
        <main id="wrapper">
            <div class="article">
                <div class="introduction">
                    <div class="[ introduction__description ] [ animate fadeInLeft ]">
                        <h1>{{ get_the_title() }}</h1>
                        {!! wpautop(get_the_excerpt()) !!}
                    </div>
                    <div class="[ introduction__thumbnail ] [ animate fadeIn ]" style="background-image: url({{ get_the_post_thumbnail_url() }})"></div>
                </div>
                <div class="article__post">
                    <div class="container__medium">
                        {!! wpautop(get_the_content()) !!}
                    </div>
                </div>
            </div>
        </main>
    @endloop
@endsection