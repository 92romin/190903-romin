@extends('layouts.main')

@section('content')
    <main id="wrapper">
        <div class="blog">
            <div class="container__large">
                <div class="[ introduction ] [ animate fadeInDown ]">
                    <h1>{{ single_term_title() }}</h1>
                    <div class="introduction__description [ animate fadeIn ]">
                        {!! wpautop(term_description()) !!}
                    </div>
                </div>

                @if($posts->have_posts())
                <div class="articles">
                    <div class="cards">
                        @php($count = 1)
                        @while($posts->have_posts())
                            @php($posts->the_post())
                            <div class="card [ animate slideInUp ]" @if($count % 2) data-wow-delay="0.25s" @endif>
                                <div class="[ card__single ]  @if($count % 2) [ rellax ] @endif" style="background-image: url('{{ the_post_thumbnail_url() }}')" data-rellax-xs-speed="0" data-rellax-mobile-speed="0" data-rellax-tablet-speed="-0.5" data-rellax-desktop-speed="-2">
                                    <div class="card__overlay"></div>
                                    <h2 class="card__heading">{{ get_the_title() }}</h2>
                                    <div class="card__description">
                                        {!! wpautop(get_the_excerpt()) !!}
                                    </div>
                                    <a href="{{ the_permalink() }}" class="button button--arrow">
                                        View Post <span class="arrow"></span>
                                    </a>
                                </div>
                            </div>
                            @php($count++)
                        @endwhile

                        <div class="articles__pagination">
                            {!! paginate_links([
                            'format' => '?page=%#%',
                            'current' => max( 1, get_query_var('page') ),
                            'total' => $posts->max_num_pages
                            ]) !!}
                        </div>
                        
                    </div>
                </div>
                @endif
            </div>
        </div>
    </main>
@endsection
