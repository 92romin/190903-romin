(function($, root, undefined) {

    $(function() {

        'use strict';

         /*
             When you scroll to a div.
         */
        let scrolledToElement = function(element)
        {
            var top = $(element).offset().top,
                height = $(element).height(),
                bottom = $(window).scrollTop() + $(window).height();

            return (top + height >= $(window).scrollTop() && top <= bottom);
        };

        /*
            Projects slider.
        */
        $('.section-projects__slides .grid').slick({
            autoplay: false,
            slidesToShow: 4,
            slidesToScroll: 1,
            adaptiveHeight: false,
            autoplaySpeed: 4500,
            arrows: true,
            rows: 0,
            prevArrow: $(".carousel__prev-projects"),
            nextArrow: $(".carousel__next-projects"),
            responsive: [
                {
                    breakpoint: 1400,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });

        /*
            Testimonials slider.
        */
        $('.section-testimonials__slides').slick({
            infinite: true,
            autoplay: false,
            autoplaySpeed: 4500,
            dots: false,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            rows: 0,
            prevArrow: $(".carousel__prev-testimonials"),
            nextArrow: $(".carousel__next-testimonials"),
        });

        /*
            Smooth Scroll.
        */
        // $.fn.smoothSectionScroll = function()
        // {
        //     // wait till page loads
        //     setTimeout(function ()
        //     {
        //         let hash = window.location.hash.replace('#', '.');
        //
        //         if(window.location.hash) {
        //             $('html, body').animate({
        //                 scrollTop: $(hash).offset().top - 50
        //             }, 700);
        //         }
        //
        //     }, 400);
        //
        //     // Stop all scrolling if the user scrolls half way.
        //     $('html, body').bind('scroll mousedown DOMMouseScroll mousewheel keyup', function(){
        //         $('html, body').stop();
        //     });
        // };
        //
        // /*
        //    When user clicks desktop header / tablet header navigation link.
        // */
        // $('a').click(function() {
        //     $.fn.smoothSectionScroll();
        // });
        //
        // $.fn.smoothSectionScroll();


        var image = document.getElementsByClassName('parallax');
        new simpleParallax(image, {
            overflow: true
        });


        /*
           Set cookies
        */
        if ($.cookie('accepted-cookies') !== "active") {
            $('.cookies').show();
        }

        $('.cookies__button').on('click',function(){
            $.cookie('accepted-cookies', 'active', { expires: 1 });
            $('.cookies').fadeOut();
        });

        /*
           Services Animation
        */

        let servicesAnimation;

        servicesAnimation = function() {

            $('.service').each(function(index, element){

                setTimeout(function () {

                    $('.service__icon', element).addClass('service__icon--tick');

                    if ( $( element ).hasClass('website-colour') ) {
                         $('.browser').toggleClass('browser--blue-shadow');
                         $('.hero__curve').toggleClass('hero__curve--white');
                         $('.website__background').toggleClass('website__background--colour');
                         $('.hero__illustration').toggleClass('hero__illustration--image');
                    }

                    if ( $( element ).hasClass('website-content') ) {
                         $('.hero__heading').toggleClass('hero__heading--show');
                         $('.hero__description').toggleClass('hero__description--show');
                    }

                }, index * 3000);
            })
        };

        let timeoutHandle;
        let animationTrigger = false;

        $( document ).scroll(function() {
            clearTimeout(timeoutHandle);

            timeoutHandle = setTimeout(function()
            {
                if (scrolledToElement('.section-services') && ! animationTrigger) {
                    servicesAnimation();
                    animationTrigger = true
                }
            }, 250);
        });

        $( document ).scroll(function() {
            if( $(this).scrollTop() >= $('.section-services').position().top - 100 ){
                $('.side-menu').addClass('side-menu--dark')
            }else {
                $('.side-menu').removeClass('side-menu--dark')
            }
        });

        var elm = document.querySelector('#main-header');
        var ms = new MenuSpy(elm);
    });

})(jQuery, this);