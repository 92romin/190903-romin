@extends('layouts.main')

@section('content')
    <img class="parallax" src="{{ app('wp.theme')->getUrl('assets/images/logo_tilt.svg') }}">
    <header id="main-header" class="side-menu">
        <nav>
            <ul>
                <li class="active">
                    <a href="#hero">
                        <span>Welcome!</span>
                    </a>
                </li>
                <li>
                    <a href="#clients">
                        <span>Our Clients</span>
                    </a>
                </li>
                <li>
                    <a href="#services">
                        <span>Our Services</span>
                    </a>
                </li>
                <li>
                    <a href="#testimonials">
                        <span>Testimonials</span>
                    </a>
                </li>
                <li>
                    <a href="#contact">
                        <span>Contact</span>
                    </a>
                </li>
            </ul>
        </nav>
    </header>
    <div class="section-hero" id="hero">
        <div class="section-hero__wrapper">

            <div class="illustration">
                <img class="illustration__image" src="{{ app('wp.theme')->getUrl('assets/images/illustration.svg') }}" alt="">
            </div>

            <div class="section-header">
                <div class="section-header__wrapper">
                    <img class="section-header__logo" src="{{ app('wp.theme')->getUrl('assets/images/logo.svg') }}" alt="">
                </div>
            </div>

            <div class="column">
                <div class="column__content">
                    <div class="section-hero__content">
                        <h1 class="section-hero__heading">
                            {{ get_field('hero_heading') }}
                        </h1>
                        <div class="section-hero__description">
                            {!! get_field('hero_description') !!}
                        </div>
                        @if(get_field('hero_cta'))
                            <a href="{{ get_field('hero_cta')['url'] }}" class="button section-hero__button" target="{{ get_field('hero_cta')['target'] }}" title="{{ get_field('hero_cta')['title'] }}">
                                {{ get_field('hero_cta')['title'] }}
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="section-projects" id="clients">

        <div class="section-projects__wrapper section-projects__wrapper--right">

            <div class="introduction">
                <div class="introduction__description">
                    {!! get_field('projects_description') !!}
                </div>
                <h2 class="introduction__heading">
                    {{ get_field('projects_heading') }}
                </h2>
            </div>

        </div>

        <div class="section-projects__wrapper--xxl">

            @if( have_rows('projects') )
                <div class="section-projects__slides">
                    <div class="grid">
                        @while ( have_rows('projects') ) @php(the_row())
                        <div class="grid__col-3">
                            @if(is_array(get_sub_field('link')))
                                <a href="{{ get_sub_field('link')['url'] }}" class="project" target="{{ get_sub_field('link')['target'] }}" title="{{ get_sub_field('link')['title'] }}">
                                    @else
                                        <div class="project">
                                            @endif
                                            <div class="project__thumbnail" style="background-image: url({{ get_sub_field('thumbnail') }})"></div>
                                            <div class="project__heading">
                                                {{ get_sub_field('heading') }}
                                            </div>
                                        @if(is_array(get_sub_field('link')))
                                </a>
                            @else
                        </div>
                        @endif
                    </div>
                    @endwhile
                </div>
        </div>
        @endif
    </div>

    @if( have_rows('projects') )
        <div class="carousel__controls">
            <div class="carousel__col-2 carousel__col-2--left">
                <div class="carousel__prev carousel__prev carousel__prev-projects">
                    <i class="fas fa-chevron-left"></i>
                </div>
            </div>
            <div class="carousel__col-2 carousel__col-2--right">
                <div class="carousel__next carousel__next carousel__next-projects">
                    <i class="fas fa-chevron-right"></i>
                </div>
            </div>
        </div>
        @endif
        </div>

        <div class="section-services" id="services">

            <div class="section-services__wrapper">

                <div class="introduction">
                    <h2 class="introduction__heading">
                        {{ get_field('services_heading') }}
                    </h2>
                    <div class="introduction__description">
                        {!! get_field('services_description') !!}
                    </div>
                </div>

                <div class="grid grid--center">
                    <div class="grid__col-2 grid__col-2--center">
                        @if( have_rows('services') )
                            <div class="services">
                                @while ( have_rows('services') ) @php(the_row())
                                    <div class="service @if(get_sub_field('animation_id')) {{ get_sub_field('animation_id') }} @endif">
                                        <span class="service__icon"></span>
                                        <div class="service__heading">
                                            {{ get_sub_field('heading')  }}
                                        </div>
                                        <div class="service__description">
                                            {!! get_sub_field('description')  !!}
                                        </div>
                                    </div>
                                @endwhile
                            </div>
                        @endif
                    </div>
                    <div class="grid__col-2">

                        <div class="browser">
                            <div class="browser__window">
                                <div class="header">
                                    <div class="header__url">
                                        <div class="header__url--text">www.mywebpage.com</div>
                                    </div>
                                </div>
                            </div>
                            <div class="browser__page">
                                <div class="website">
                                    <div class="website__background"></div>
                                    <div class="hero">
                                        <div class="hero__curve"></div>
                                        <div class="hero__wrapper">
                                            <div class="grid">
                                                <div class="grid__col-2">
                                                    <div class="hero__heading">
                                                        Welcome to my website!
                                                    </div>
                                                    <div class="hero__description">
                                                        Have a look at our products
                                                        & services.
                                                    </div>
                                                </div>
                                                <div class="grid__col-2">
                                                    <div class="hero__illustration"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-testimonials" id="testimonials">
            <h2 class="section-testimonials__heading">
                {{ get_field('testimonials_heading') }}
            </h2>
            <div class="section-testimonials__description">
                {!! get_field('testimonials_description') !!}
            </div>

            <div class="section-testimonials__wrapper">
                @if( have_rows('testimonials') )
                    <div class="section-testimonials__slides">
                        @while ( have_rows('testimonials') ) @php(the_row())
                        <div>
                            <div class="column">
                                <div class="column__profile">
                                    <div class="client">
                                        <div class="client__thumbnail" style="background-image: url({{ get_sub_field('thumbnail') }})"></div>
                                        <div class="client__name">
                                            {{ get_sub_field('client') }}
                                        </div>
                                        <div class="client__role">
                                            {{ get_sub_field('role') }}
                                        </div>
                                    </div>
                                </div>

                                <div class="column__content">
                                    <div class="client__testimonial">
                                        {!!  get_sub_field('testimonial') !!}
                                    </div>
                                </div>
                            </div>

                        </div>
                        @endwhile
                    </div>
                @endif
            </div>

            @if( have_rows('testimonials') )
                <div class="carousel__controls">
                    <div class="carousel__col-2 carousel__col-2--left">
                        <div class="carousel__prev carousel__prev carousel__prev-testimonials">
                            <i class="fas fa-chevron-left"></i>
                        </div>
                    </div>
                    <div class="carousel__col-2 carousel__col-2--right">
                        <div class="carousel__next carousel__next carousel__next-testimonials">
                            <i class="fas fa-chevron-right"></i>
                        </div>
                    </div>
                </div>
            @endif
        </div>


        <div class="section-contact" id="contact">
            <h2 class="section-contact__heading">
                {{ get_field('contact_heading') }}
            </h2>
            <div class="section-contact__description">
                {!! get_field('contact_description') !!}
            </div>

            <div class="section-contact__wrapper">
                <div class="form">
                    <?php gravity_form( 1, false, true, true, '', true ); ?>
                </div>
            </div>
        </div>

        <div class="section-footer">
            <div class="section-footer__wrapper">
                <div class="company">
                    <img class="company__logo" src="{{ app('wp.theme')->getUrl('assets/images/logo_white.svg') }}" fill="purple" alt="">

                    <div class="social">

                        @if( have_rows('social_media', 'options') )

                            @while ( have_rows('social_media', 'options') ) @php(the_row())

                            <a href="{{ get_sub_field('link') }}" target="_blank" class="social__platform">
                                @if(get_sub_field('platform') == 'facebook')
                                    <i class="fab fa-facebook-f"></i>
                                @elseif(get_sub_field('platform') == 'instagram')
                                    <i class="fab fa-instagram"></i>
                                @endif
                            </a>

                            @endwhile
                        @endif
                    </div>
                </div>
                <div class="section-footer__copyright">
                    Copyright &copy; 2019 | Designed & Developed by Romin
                </div>
            </div>
        </div>

        <div class="cookies">
            <div class="cookies__wrapper">
                <div class="column">
                    <div class="column__content">
                        <div class="cookies__message">
                            We use cookies to ensure that we give you the best experience on our website.
                        </div>
                    </div>
                    <div class="column__button">
                    <span class="cookies__button">
                        Accept & Close
                    </span>
                    </div>
                </div>
            </div>
        </div>
@endsection