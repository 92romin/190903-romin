<?php

use Themosis\Support\Facades\Filter;

/**
 * Enqueue Fontawesome
 */
Asset::add('font-awesome-free', '//use.fontawesome.com/releases/v5.8.1/css/all.css', [], '5.8.1', 'all')->to('front');
Asset::add('slick-carousel', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css', [], '1.9.0', 'all')->to('front');
Asset::add('animate', '//cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.1/animate.min.css', [], '3.7.1', 'all')->to('front');

/**
 * Enqueue JQuery 
 */
Asset::add('menu-spy', get_template_directory_uri(). '/dist/js/menuspy.min.js', ['jquery'], '5.1.0', true)->to('front');
Asset::add('simple-parallax-js', '//cdn.jsdelivr.net/npm/simple-parallax-js@5.0.2/dist/simpleParallax.min.js', ['jquery'], '5.1.0', true)->to('front');
Asset::add('slick', '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js', ['jquery'], '1.9.0', true)->to('front');
Asset::add('wow', '//cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js', ['jquery'], '1.1.2', true)->to('front');
Asset::add('jquery-cookie', '//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js', ['jquery'], '1.4.1', true)->to('front');
Asset::add('jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', ['jquery'], '3.4.1', true)->to('front');

/**
 * Remove custom actions from the header
 */
Action::remove('wp_head', 'print_emoji_detection_script', 7);
Action::remove('wp_print_styles', 'print_emoji_styles');
Action::remove( 'admin_print_scripts', 'print_emoji_detection_script' );
Action::remove( 'admin_print_styles', 'print_emoji_styles' );

/**
 * Add ACF options page.
 */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

/**
 * Save ACF json file.
 */
Filter::add('acf/settings/save_json', function ($paths) {
    return get_stylesheet_directory() . '/acf-json';
});

/**
 * Load ACF json file.
 */
Filter::add('acf/settings/load_json', function ($paths) {
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/acf-json';
    return $paths;
});
